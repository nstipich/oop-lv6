﻿namespace Kalkulator
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Cbutt = new System.Windows.Forms.Button();
            this.sevbutt = new System.Windows.Forms.Button();
            this.foubutt = new System.Windows.Forms.Button();
            this.onebutt = new System.Windows.Forms.Button();
            this.zerbutt = new System.Windows.Forms.Button();
            this.eigbutt = new System.Windows.Forms.Button();
            this.fivbutt = new System.Windows.Forms.Button();
            this.twobutt = new System.Windows.Forms.Button();
            this.sembutt = new System.Windows.Forms.Button();
            this.ninbutt = new System.Windows.Forms.Button();
            this.sixbutt = new System.Windows.Forms.Button();
            this.thrbutt = new System.Windows.Forms.Button();
            this.equbutt = new System.Windows.Forms.Button();
            this.plubutt = new System.Windows.Forms.Button();
            this.minbutt = new System.Windows.Forms.Button();
            this.mulbutt = new System.Windows.Forms.Button();
            this.divbutt = new System.Windows.Forms.Button();
            this.sqrtbutt = new System.Windows.Forms.Button();
            this.sinbutt = new System.Windows.Forms.Button();
            this.cosbutt = new System.Windows.Forms.Button();
            this.tanbutt = new System.Windows.Forms.Button();
            this.logbutt = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // Cbutt
            // 
            this.Cbutt.Location = new System.Drawing.Point(9, 74);
            this.Cbutt.Name = "Cbutt";
            this.Cbutt.Size = new System.Drawing.Size(45, 23);
            this.Cbutt.TabIndex = 0;
            this.Cbutt.Text = "C";
            this.Cbutt.UseVisualStyleBackColor = true;
            this.Cbutt.Click += new System.EventHandler(this.Cbutt_Click);
            // 
            // sevbutt
            // 
            this.sevbutt.Location = new System.Drawing.Point(9, 119);
            this.sevbutt.Name = "sevbutt";
            this.sevbutt.Size = new System.Drawing.Size(46, 23);
            this.sevbutt.TabIndex = 1;
            this.sevbutt.Text = "7";
            this.sevbutt.UseVisualStyleBackColor = true;
            this.sevbutt.Click += new System.EventHandler(this.button_click);
            // 
            // foubutt
            // 
            this.foubutt.Location = new System.Drawing.Point(10, 169);
            this.foubutt.Name = "foubutt";
            this.foubutt.Size = new System.Drawing.Size(45, 23);
            this.foubutt.TabIndex = 2;
            this.foubutt.Text = "4";
            this.foubutt.UseVisualStyleBackColor = true;
            this.foubutt.Click += new System.EventHandler(this.button_click);
            // 
            // onebutt
            // 
            this.onebutt.Location = new System.Drawing.Point(9, 215);
            this.onebutt.Name = "onebutt";
            this.onebutt.Size = new System.Drawing.Size(46, 23);
            this.onebutt.TabIndex = 3;
            this.onebutt.Text = "1";
            this.onebutt.UseVisualStyleBackColor = true;
            this.onebutt.Click += new System.EventHandler(this.button_click);
            // 
            // zerbutt
            // 
            this.zerbutt.Location = new System.Drawing.Point(10, 254);
            this.zerbutt.Name = "zerbutt";
            this.zerbutt.Size = new System.Drawing.Size(46, 23);
            this.zerbutt.TabIndex = 4;
            this.zerbutt.Text = "0";
            this.zerbutt.UseVisualStyleBackColor = true;
            this.zerbutt.Click += new System.EventHandler(this.button_click);
            // 
            // eigbutt
            // 
            this.eigbutt.Location = new System.Drawing.Point(76, 119);
            this.eigbutt.Name = "eigbutt";
            this.eigbutt.Size = new System.Drawing.Size(43, 23);
            this.eigbutt.TabIndex = 5;
            this.eigbutt.Text = "8";
            this.eigbutt.UseVisualStyleBackColor = true;
            this.eigbutt.Click += new System.EventHandler(this.button_click);
            // 
            // fivbutt
            // 
            this.fivbutt.Location = new System.Drawing.Point(76, 169);
            this.fivbutt.Name = "fivbutt";
            this.fivbutt.Size = new System.Drawing.Size(43, 23);
            this.fivbutt.TabIndex = 6;
            this.fivbutt.Text = "5";
            this.fivbutt.UseVisualStyleBackColor = true;
            this.fivbutt.Click += new System.EventHandler(this.button_click);
            // 
            // twobutt
            // 
            this.twobutt.Location = new System.Drawing.Point(76, 215);
            this.twobutt.Name = "twobutt";
            this.twobutt.Size = new System.Drawing.Size(43, 23);
            this.twobutt.TabIndex = 7;
            this.twobutt.Text = "2";
            this.twobutt.UseVisualStyleBackColor = true;
            this.twobutt.Click += new System.EventHandler(this.button_click);
            // 
            // sembutt
            // 
            this.sembutt.Location = new System.Drawing.Point(76, 253);
            this.sembutt.Name = "sembutt";
            this.sembutt.Size = new System.Drawing.Size(43, 24);
            this.sembutt.TabIndex = 8;
            this.sembutt.Text = ",";
            this.sembutt.UseVisualStyleBackColor = true;
            this.sembutt.Click += new System.EventHandler(this.button_click);
            // 
            // ninbutt
            // 
            this.ninbutt.Location = new System.Drawing.Point(149, 119);
            this.ninbutt.Name = "ninbutt";
            this.ninbutt.Size = new System.Drawing.Size(43, 23);
            this.ninbutt.TabIndex = 9;
            this.ninbutt.Text = "9";
            this.ninbutt.UseVisualStyleBackColor = true;
            this.ninbutt.Click += new System.EventHandler(this.button_click);
            // 
            // sixbutt
            // 
            this.sixbutt.Location = new System.Drawing.Point(149, 169);
            this.sixbutt.Name = "sixbutt";
            this.sixbutt.Size = new System.Drawing.Size(43, 23);
            this.sixbutt.TabIndex = 10;
            this.sixbutt.Text = "6";
            this.sixbutt.UseVisualStyleBackColor = true;
            this.sixbutt.Click += new System.EventHandler(this.button_click);
            // 
            // thrbutt
            // 
            this.thrbutt.Location = new System.Drawing.Point(149, 215);
            this.thrbutt.Name = "thrbutt";
            this.thrbutt.Size = new System.Drawing.Size(43, 23);
            this.thrbutt.TabIndex = 11;
            this.thrbutt.Text = "3";
            this.thrbutt.UseVisualStyleBackColor = true;
            this.thrbutt.Click += new System.EventHandler(this.button_click);
            // 
            // equbutt
            // 
            this.equbutt.Location = new System.Drawing.Point(149, 254);
            this.equbutt.Name = "equbutt";
            this.equbutt.Size = new System.Drawing.Size(43, 23);
            this.equbutt.TabIndex = 12;
            this.equbutt.Text = "=";
            this.equbutt.UseVisualStyleBackColor = true;
            this.equbutt.Click += new System.EventHandler(this.equbutt_Click);
            // 
            // plubutt
            // 
            this.plubutt.Location = new System.Drawing.Point(209, 119);
            this.plubutt.Name = "plubutt";
            this.plubutt.Size = new System.Drawing.Size(43, 23);
            this.plubutt.TabIndex = 13;
            this.plubutt.Text = "+";
            this.plubutt.UseVisualStyleBackColor = true;
            this.plubutt.Click += new System.EventHandler(this.operator_click);
            // 
            // minbutt
            // 
            this.minbutt.Location = new System.Drawing.Point(209, 169);
            this.minbutt.Name = "minbutt";
            this.minbutt.Size = new System.Drawing.Size(43, 23);
            this.minbutt.TabIndex = 14;
            this.minbutt.Text = "-";
            this.minbutt.UseVisualStyleBackColor = true;
            this.minbutt.Click += new System.EventHandler(this.operator_click);
            // 
            // mulbutt
            // 
            this.mulbutt.Location = new System.Drawing.Point(209, 215);
            this.mulbutt.Name = "mulbutt";
            this.mulbutt.Size = new System.Drawing.Size(43, 23);
            this.mulbutt.TabIndex = 15;
            this.mulbutt.Text = "*";
            this.mulbutt.UseVisualStyleBackColor = true;
            this.mulbutt.Click += new System.EventHandler(this.operator_click);
            // 
            // divbutt
            // 
            this.divbutt.Location = new System.Drawing.Point(209, 254);
            this.divbutt.Name = "divbutt";
            this.divbutt.Size = new System.Drawing.Size(43, 23);
            this.divbutt.TabIndex = 16;
            this.divbutt.Text = "/";
            this.divbutt.UseVisualStyleBackColor = true;
            this.divbutt.Click += new System.EventHandler(this.operator_click);
            // 
            // sqrtbutt
            // 
            this.sqrtbutt.Location = new System.Drawing.Point(277, 74);
            this.sqrtbutt.Name = "sqrtbutt";
            this.sqrtbutt.Size = new System.Drawing.Size(43, 23);
            this.sqrtbutt.TabIndex = 17;
            this.sqrtbutt.Text = "sqrt";
            this.sqrtbutt.UseVisualStyleBackColor = true;
            this.sqrtbutt.Click += new System.EventHandler(this.button18_Click);
            // 
            // sinbutt
            // 
            this.sinbutt.Location = new System.Drawing.Point(277, 119);
            this.sinbutt.Name = "sinbutt";
            this.sinbutt.Size = new System.Drawing.Size(43, 23);
            this.sinbutt.TabIndex = 18;
            this.sinbutt.Text = "sin";
            this.sinbutt.UseVisualStyleBackColor = true;
            this.sinbutt.Click += new System.EventHandler(this.sinbutt_Click);
            // 
            // cosbutt
            // 
            this.cosbutt.Location = new System.Drawing.Point(277, 169);
            this.cosbutt.Name = "cosbutt";
            this.cosbutt.Size = new System.Drawing.Size(43, 23);
            this.cosbutt.TabIndex = 19;
            this.cosbutt.Text = "cos";
            this.cosbutt.UseVisualStyleBackColor = true;
            this.cosbutt.Click += new System.EventHandler(this.cosbutt_Click);
            // 
            // tanbutt
            // 
            this.tanbutt.Location = new System.Drawing.Point(277, 215);
            this.tanbutt.Name = "tanbutt";
            this.tanbutt.Size = new System.Drawing.Size(43, 23);
            this.tanbutt.TabIndex = 20;
            this.tanbutt.Text = "tan";
            this.tanbutt.UseVisualStyleBackColor = true;
            this.tanbutt.Click += new System.EventHandler(this.tanbutt_Click);
            // 
            // logbutt
            // 
            this.logbutt.Location = new System.Drawing.Point(277, 254);
            this.logbutt.Name = "logbutt";
            this.logbutt.Size = new System.Drawing.Size(43, 23);
            this.logbutt.TabIndex = 21;
            this.logbutt.Text = "log";
            this.logbutt.UseVisualStyleBackColor = true;
            this.logbutt.Click += new System.EventHandler(this.logbutt_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(76, 23);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(176, 20);
            this.textBox1.TabIndex = 22;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(332, 303);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.logbutt);
            this.Controls.Add(this.tanbutt);
            this.Controls.Add(this.cosbutt);
            this.Controls.Add(this.sinbutt);
            this.Controls.Add(this.sqrtbutt);
            this.Controls.Add(this.divbutt);
            this.Controls.Add(this.mulbutt);
            this.Controls.Add(this.minbutt);
            this.Controls.Add(this.plubutt);
            this.Controls.Add(this.equbutt);
            this.Controls.Add(this.thrbutt);
            this.Controls.Add(this.sixbutt);
            this.Controls.Add(this.ninbutt);
            this.Controls.Add(this.sembutt);
            this.Controls.Add(this.twobutt);
            this.Controls.Add(this.fivbutt);
            this.Controls.Add(this.eigbutt);
            this.Controls.Add(this.zerbutt);
            this.Controls.Add(this.onebutt);
            this.Controls.Add(this.foubutt);
            this.Controls.Add(this.sevbutt);
            this.Controls.Add(this.Cbutt);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Cbutt;
        private System.Windows.Forms.Button sevbutt;
        private System.Windows.Forms.Button foubutt;
        private System.Windows.Forms.Button onebutt;
        private System.Windows.Forms.Button zerbutt;
        private System.Windows.Forms.Button eigbutt;
        private System.Windows.Forms.Button fivbutt;
        private System.Windows.Forms.Button twobutt;
        private System.Windows.Forms.Button sembutt;
        private System.Windows.Forms.Button ninbutt;
        private System.Windows.Forms.Button sixbutt;
        private System.Windows.Forms.Button thrbutt;
        private System.Windows.Forms.Button equbutt;
        private System.Windows.Forms.Button plubutt;
        private System.Windows.Forms.Button minbutt;
        private System.Windows.Forms.Button mulbutt;
        private System.Windows.Forms.Button divbutt;
        private System.Windows.Forms.Button sqrtbutt;
        private System.Windows.Forms.Button sinbutt;
        private System.Windows.Forms.Button cosbutt;
        private System.Windows.Forms.Button tanbutt;
        private System.Windows.Forms.Button logbutt;
        private System.Windows.Forms.TextBox textBox1;
    }
}

