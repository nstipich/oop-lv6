﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kalkulator
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        private string operand ="";
        private double result = 0;
        

        private void button18_Click(object sender, EventArgs e)
        {
            textBox1.Text = Math.Sqrt(Double.Parse(textBox1.Text)).ToString();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button_click(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            textBox1.Text = textBox1.Text + button.Text;
        }

        private void Cbutt_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
        }

        private void operator_click(object sender, EventArgs e)
        {
            Button button2 = (Button)sender;
            operand = button2.Text;
            result = double.Parse(textBox1.Text);
            textBox1.Clear();
        }

        private void equbutt_Click(object sender, EventArgs e)
        {
            switch (operand)
            {
                case "+":
                    textBox1.Text = (result + double.Parse(textBox1.Text)).ToString();
                    break;
                case "-":
                    textBox1.Text = (result - double.Parse(textBox1.Text)).ToString();
                    break;
                case "*":
                    textBox1.Text = (result * double.Parse(textBox1.Text)).ToString();
                    break;
                case "/":
                    textBox1.Text = (result / double.Parse(textBox1.Text)).ToString();
                    break;
            }

        }

        private void sinbutt_Click(object sender, EventArgs e)
        {
            textBox1.Text = Math.Sin(Double.Parse(textBox1.Text)).ToString();
        }

        private void cosbutt_Click(object sender, EventArgs e)
        {
            textBox1.Text = Math.Cos(Double.Parse(textBox1.Text)).ToString();
        }

        private void tanbutt_Click(object sender, EventArgs e)
        {
            textBox1.Text = Math.Tan(Double.Parse(textBox1.Text)).ToString();
        }

        private void logbutt_Click(object sender, EventArgs e)
        {
            textBox1.Text = Math.Log10(Double.Parse(textBox1.Text)).ToString();
        }
    }
}
