﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Vješala.Properties;

namespace Vješala
{

    public partial class Form1 : Form
    {
        public string X;
        public int Y;
        public char[] Z;


        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {


            Random rnd = new Random();
            List<Pojam> pojmovi = new List<Pojam>();

            using (System.IO.StreamReader reader = new System.IO.StreamReader("pojmovi.txt"))
            {
                string line;
                while ((line = reader.ReadLine()) != null) // čitanje svih linija iz dat.
                {
                    Pojam P = new Pojam(line); // novi objekt
                    pojmovi.Add(P); // umetanje objekta u listu
                }
            }

            int r = rnd.Next(pojmovi.Count);
            X = pojmovi[r].ToString();
            Z = new char[X.Length];
            Y = 6;
            label2.Text = X.Length.ToString();
            label4.Text = Y.ToString();
            for(int i=0; i<X.Length; i++)
            {
                Z[i]='_';
            }
            label5.Text = new string(Z);
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(textBox1.Text==""){
                MessageBox.Show("Unesite slovo.");
            }
            else if(textBox1.Text.Length > 1){
                MessageBox.Show("Unesite jedno slovo.");
            }
            else
            {
                int a = 0, b=0;
                for(int i =0; i<X.Length; i++)
                {
                    if (X[i] == textBox1.Text[0])
                    {
                        Z[i] = textBox1.Text[0];
                        label5.Text = new string(Z);
                        a = 1;
                    }
                    else
                    {
                        label4.Text = Y.ToString();
                    }
                }
                if (a == 0)
                {
                    Y--;
                    label4.Text = Y.ToString();
                    if(Y == 0)
                    {
                        MessageBox.Show("Izgubio si.", "Kraj");
                    }
                }
                else if (a==1)
                {
                    for(int i=0; i<X.Length; i++)
                    {
                        if (Z[i] == '_')
                        {
                            b = 1;
                        }
                    }
                    if (b == 0)
                    {
                        MessageBox.Show("Pobjedio si", "Uspjeh");
                    }
                }
            }
        }
    }
    }
