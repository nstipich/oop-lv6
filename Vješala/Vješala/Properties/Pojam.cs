﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vješala.Properties
{
    class Pojam
    {
        private string pojam;

        public Pojam()
        {
            pojam = "Drvo";
        }
        public Pojam(string x)
        {
            pojam = x;
        }
        public override string ToString()
        {
            return pojam;
        }

    }
}
